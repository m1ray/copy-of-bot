from aiogram import Bot, Dispatcher, executor, types
from aiogram.dispatcher.filters.state import StatesGroup, State
from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from aiogram.dispatcher import FSMContext
from aiogram.contrib.fsm_storage.memory import MemoryStorage
import keyboards
import MESSAGES
import config
import datetime

from sqlite import db_start, check_user, add_user, check_subs, add_dev, check_count_dev

storage = MemoryStorage()
bot = Bot(config.TOKEN_API, parse_mode=types.ParseMode.HTML)
dp = Dispatcher(bot, storage=MemoryStorage())




class StatePromocode(StatesGroup):
    waiting_for_promocode = State()


async def on_startup(_):
    '''
    Запускает бд
    :param _:
    :return:
    '''
    print("hello")
    await db_start()


@dp.message_handler(commands=['start'], state='*')
async def send_reply_start(message: types.Message):
    '''
    Запускается при команде start, далее проверяет первый раз ли пользователь зашел в бота,
    если в первый
    :param message:
    :return:
    '''
    if await check_user(user_id=message.chat.id) == 0:
        await message.answer_photo(photo=config.welcome_logo,
                                   caption=MESSAGES.first_start_message,
                                   reply_markup=await keyboards.get_first_start_message_keyboard())
        await add_user(user_id=message.chat.id)
    else:
        # Проверяется есть ли у пользователя подписка
        subs = check_subs(user_id=message.from_user.id)
        if subs == 'yes':
            #случай когда есть
            activated = True
            await message.answer(MESSAGES.main_menu_header, reply_markup=await keyboards.get_keyboard_for_menu(activated))
        else:
            activated = False
            #случай когда нету
            await message.answer(MESSAGES.main_menu_header,
                                       reply_markup=await keyboards.get_keyboard_for_menu(activated))


@dp.callback_query_handler(text="second_start_message")
async def send_reply_second_start(query: types.CallbackQuery):
    '''
    Отправляет сообщение и инлайн клавиатуру
    :param query:
    :return:
    '''
    await query.message.answer(text=MESSAGES.second_start_message,
                               reply_markup=await keyboards.get_second_start_message_keyboard())


@dp.callback_query_handler(text="menu")
async def menu(query: types.CallbackQuery):
    '''
    Открывает меню с инлайн клавиатурой
    :param query:
    :return:
    '''
    subs = check_subs(user_id=query.message.chat.id)
    if subs == 'yes':
        activated = True
        await query.message.answer(MESSAGES.main_menu_header,
                               reply_markup=await keyboards.get_keyboard_for_menu(activated))
    else:
        activated = False
        await query.message.answer(MESSAGES.main_menu_header,
                                   reply_markup=await keyboards.get_keyboard_for_menu(activated))



@dp.callback_query_handler(text="info")
async def info_button(query: types.CallbackQuery):
    '''
    Отправляет сообщение с информацией и инлайн клавиатуру
    :param query:
    :return:
    '''
    await query.message.delete()
    await query.message.answer(MESSAGES.info_header,
                               reply_markup=await keyboards.get_info_keyboard()
                               )

    # await PlansStatesGroup.info.set()


@dp.callback_query_handler(text="support")
async def send_support_button(query: types.CallbackQuery):
    '''
    Отправляет сообщение о том, как связаться с поддержкой, и инлайн клавиатуру
    :param query:
    :return:
    '''
    await query.message.delete()
    await query.message.answer(MESSAGES.support_header,
                               reply_markup=await keyboards.get_support_button()
                               )


@dp.callback_query_handler(text="benefits")
async def benefits_button(query: types.CallbackQuery):
    '''
    Не помню потом допишу
    :param query:
    :return:
    '''
    await query.message.delete()
    await query.message.answer(MESSAGES.benefits_header + MESSAGES.benefits,
                               reply_markup=await keyboards.get_benefits())


@dp.callback_query_handler(text="FAQ")
async def send_faq(query: types.CallbackQuery):
    '''
    Отправяет сообщение с faq и инлайн клавиатуру
    :param query:
    :return:
    '''
    await query.message.delete()
    await query.message.answer(MESSAGES.FAQ,
                               reply_markup=await keyboards.FAQ(),
                               )


@dp.callback_query_handler(text="install")
async def send_how_to_install(query: types.CallbackQuery):
    '''
    Отправляет инструкцию загрузки на разные устройства и инлайн клавиатуру
    :param query:
    :return:
    '''
    await query.message.delete()
    await bot.send_message(
        query.message.chat.id,
        reply_markup=await keyboards.get_install(),
        text='Инструкции для разных устройств',
        disable_web_page_preview=True
    )


@dp.callback_query_handler(text="install_pc")
async def send_install_pc(query: types.CallbackQuery):
    '''
    Отправляет инструкцию для пк
    :param query:
    :return:
    '''
    await query.message.delete()
    await query.message.answer(MESSAGES.instruction_pc,
                               reply_markup=await keyboards.get_menu(),
                               )


@dp.callback_query_handler(text="install_phone")
async def install_button_phone(query: types.CallbackQuery):
    '''
        Отправляет инструкцию для телефона
        :param query:
        :return:
    '''
    await query.message.delete()
    await query.message.answer(MESSAGES.instruction_phone,
                               reply_markup=await keyboards.get_menu(),
                               )


@dp.callback_query_handler(text='subscription_manage')
async def subscription_manage(query: types.CallbackQuery):
    '''
    Отправляет сообщение о том, что нет аккаунта
    :param query:
    :return:
    '''
    await query.message.delete()
    #тут проверяется есть/нет/была подписка
    subs = check_subs(user_id=query.message.chat.id)
    if subs == 'yes': #есть подписка
        await query.message.answer('Здесь вы можете управлять вашими подписками',reply_markup=await keyboards.subs_info())
    elif subs == 'was': #была подписка
        date = '23.03.23'#запрос в бд
        duration = 'месяц'# запрос в бд
        await query.message.answer(f'Ваша подписка на {duration} закончилась {date}', reply_markup=await keyboards.subs_ended())
    else: #нет подписки
        await query.message.answer(MESSAGES.subscription_absence,
                               reply_markup=await keyboards.account_denied(),)


@dp.callback_query_handler(text=['plans_menu', 'plans_info', 'plans_account', 'plans_renew', 'plans_subs'])
async def subscriptions(query: types.CallbackQuery):

    button = query.data  # узнаем какой текст нам пришел в хендлер

    place_of_request = ''
    if button == 'plans_menu':
        place_of_request = 'menu'
    elif button == 'plans_info':
        place_of_request = 'info'
    elif button == 'plans_account':
        place_of_request = 'account'
    elif button == 'plans_renew':
        place_of_request = 'renew_subs'
    elif button == 'plans_subs':
        place_of_request = 'subs_info'

    await query.message.delete()
    if place_of_request == 'menu' or place_of_request == 'info' or place_of_request == 'account':
        await query.message.answer(MESSAGES.plans,
                                   reply_markup=await keyboards.subscriptions_with_promocode(place_of_request))
    else:
        await query.message.answer(MESSAGES.plans,
                                   reply_markup=await keyboards.subscriptions(
                                       place_of_request))



@dp.callback_query_handler(text='user_has_promo' )
async def use_promo(query: types.CallbackQuery):
    await query.message.answer('Введите промокод')
    await StatePromocode.waiting_for_promocode.set()


@dp.message_handler(lambda message: (message.text == 0) or message.text not in ['1234567', 'crossboardervpn'],
                    state=StatePromocode.waiting_for_promocode)
async def wrong_promocode(message: types.Message):
    await message.reply('Ваш промокод не подходит')


@dp.message_handler(state=StatePromocode.waiting_for_promocode)
async def get_promocode(message: types.Message, state: FSMContext):
    '''
    надо будет допилить бд(на ядре??), где будут хранится промокоды
    :param message:
    :return:
    '''
    if message.text in ['1234567']:  # бесплатный сервер на месяц
        await message.answer(MESSAGES.free_server,
                             reply_markup=await keyboards.choose_device_forfreeserver())
    elif message.text in ['crossboardervpn']:  # скидка на тарифы
        await message.answer(MESSAGES.plans_with_sale,
                             reply_markup=await keyboards.subscriptions_with_promocode())
    await state.finish()


@dp.callback_query_handler(text=['phone_tablet_free', 'desktop_laptop_free'])
async def send_freeserver_qr(query: types.CallbackQuery):
    await query.message.answer_photo(photo=config.sample_qr,
                             reply_markup=await keyboards.qrcode_freeserver())


    @dp.callback_query_handler(text=['subscription_for_one_month', 'subscription_for_six_months', 'subscription_for_twelve_months'])
async def get_subscribe(query: types.CallbackQuery):
    sub_duration = query.data
    if sub_duration == 'subscription_for_one_month':
        pass
    elif sub_duration == 'subscription_for_six_months':
        pass
    elif sub_duration == 'subscription_for_twelve_months':
        pass

    await query.message.answer(MESSAGES.recc_subscription,
                               reply_markup=await keyboards.recc_subs())

@dp.callback_query_handler(text=['recc_subs_yes', 'recc_subs_not'])
async def recc_subs(query: types.CallbackQuery):
    '''
    тут будет происходить оплата
    :param query:
    :return:
    '''

    await query.message.answer('Оплата прошла?',
                               reply_markup= await keyboards.succesfull_buy())


@dp.callback_query_handler(text=['succesfull', 'add_new_device'])
async def choose_device(query: types.CallbackQuery):
    counter = check_count_dev(query.message.chat.id)
    await query.message.answer(MESSAGES.choose_deivce,
                               reply_markup=await keyboards.choose_device())
    if counter<=5:
        add_dev(query.message.chat.id)

@dp.callback_query_handler(text=['phone_tablet', 'desktop_laptop', 'subs_menu'])
async def send_config_qr(query: types.CallbackQuery):
    device = query.data
    if device == 'desktop_laptop':
        await query.message.answer("ТУТ БУДЕТ КОНФИГ В ВИДЕ ФАЙЛА")
    elif device == 'phone_tablet':
        await query.message.answer_photo(config.sample_qr)
    await query.message.answer(text='Теперь в вы в меню управления подписками', reply_markup=await keyboards.subs_menu())

@dp.callback_query_handler(text="instruction")
async def get_instruction_button(query: types.CallbackQuery):
    await query.message.answer('Здесь вы можете ознакомиться с инструкциями',
                               reply_markup=await keyboards.instruction_button())

@dp.callback_query_handler(text=["instruction_pc",'instruction_phone'])
async def get_instruction_button(query: types.CallbackQuery):
    instruction = query.data
    if instruction == 'instruction_pc':
        await query.message.answer(MESSAGES.instruction_pc, reply_markup= await keyboards.get_menu())
    elif instruction == 'instruction_phone':
        await query.message.answer(MESSAGES.instruction_phone, reply_markup=await keyboards.get_menu())


@dp.callback_query_handler(text='device_management')
async def manage_devices(query: types.CallbackQuery):
    '''
    тут еще должна быть проверка сколько устройств подключено
    :param query:
    :return:
    '''
    counter = check_count_dev(query.message.chat.id)
    await query.message.answer(text=f'У вас подключено {counter} устройство\nДоступно {5 - counter} новых подключения ', reply_markup=await keyboards.devices(counter))


# @dp.callback_query_handler(text='check_first_device')
# async def manage_devices(query: types.CallbackQuery):
#     count_dev = query.data
#     if count_dev == 'check_first_device':
#         await query.message.answer(text='Устройство 1', reply_markup=await keyboards.change_name('newname'))


@dp.callback_query_handler(text='subscription_info')
async def subscription_info(query: types.CallbackQuery):
    activated = True
    counter = check_count_dev(query.message.chat.id)
    await query.message.answer('Здесь вы можете управлять вашими подписками',reply_markup=await keyboards.subs_info(counter))


@dp.callback_query_handler(text='info_about_subscription')
async def inforamation_about_duration(query: types.CallbackQuery):
    date = '23.04.23' #брать из бд
    count  = 5 #брать из бд
    text = f'Ваша подписка действует до {date}. Доступно до {count} устройств'
    await query.message.answer(text,
                               reply_markup=await keyboards.get_back('subscription_info'))
@dp.callback_query_handler(text='renew_subs')
async def renew_subs(query: types.CallbackQuery):
    await query.message.answer(text='Ваша подписка успешно продлена до 23.05.23', reply_markup=await keyboards.get_menu())

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True, on_startup=on_startup)

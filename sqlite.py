import sqlite3 as sq


async def db_start():
    '''
    Создает файл базы данных(если его нет) и таблицу users, в которой хранится id чата пользователя, количетство
    подписок и есть ли у пользователя подписка(0, если нет, 1, если была, 2, если есть)
    :return: None
    '''
    global db, cur

    db = sq.connect('new.db')
    cur = db.cursor()

    cur.execute("CREATE TABLE IF NOT EXISTS users(user_id TEXT PRIMARYKEY, count_dev INTEGER DEFAULT 0, subs_available INTEGER)")
    # cur.execute("CREATE TABLE IF NOT EXISTS dev_names(user_id TEXT PRIMARYKEY, first_dev TEXT,second_dev TEXT, third_dev TEXT, fourth_dev TEXT, fifth_dev TEXT)")
    db.commit()


async def check_user(user_id: str):
    '''
    Проверяет есть ли пользователь в бд
    :param user_id: str
    :return:
    '''
    user = cur.execute("SELECT 1 FROM users WHERE user_id == {key}".format(key=user_id)).fetchone()
    if user:
        return 1
    return 0


async def add_user(user_id: str):
    '''
    Добавляет id чата в бд, если его там нет
    :param user_id:
    :return:
    '''

    query = f"INSERT INTO users (user_id) VALUES ({user_id})"
    cur.execute(query)
    db.commit()


def check_subs(user_id: str):
    '''
    Проверяет есть/была ли у пользователя подписка, если есть возвращает 'yes', если была - 'was', иначе 'no'
    (в бд хранятся 0('no'), 1('was),2('yes')
    скорее всего тестовая функция, потому что эта инфа будет хранится на ядре
    :param user_id: str
    :return: yes/was/no
    '''
    subs = cur.execute("SELECT subs_available FROM users WHERE user_id=?", (user_id,)).fetchone()
    if subs[0] == 1:
        return 'was'
    elif subs[0] == 2:
        return 'yes'
    else:
        return 'no'

# def add_dev(user_id: str, dev_number: str, ):
#
#     query = f"INSERT INTO dev_names ({dev_number})"

def add_dev(user_id: str):
    counter = cur.execute("SELECT count_dev FROM users WHERE user_id=?", (user_id,)).fetchone()[0]
    counter += 1
    cur.execute("UPDATE users SET count_dev=? WHERE user_id=?", (counter, user_id))
    db.commit()
    if counter == 5:
        return 'LIMIT 5'
    return 1

def check_count_dev(user_id: str):
    counter = cur.execute("SELECT count_dev FROM users WHERE user_id=?", (user_id,)).fetchone()[0]
    return int(counter)





from aiogram.types import ReplyKeyboardMarkup, KeyboardButton
from aiogram.types import InlineKeyboardButton, InlineKeyboardMarkup
import MESSAGES


async def get_first_start_message_keyboard():
    # Create an object of type "InlineKeyboardMarkup"
    keyboard = InlineKeyboardMarkup()
    keyboard.row(InlineKeyboardButton(text='Круто, а как это работает?', callback_data='second_start_message'))
    return keyboard


async def get_second_start_message_keyboard():
    # Create an object of type "InlineKeyboardMarkup"
    keyboard = InlineKeyboardMarkup()

    # Create a row of buttons on the keyboard
    keyboard.row(
        InlineKeyboardButton(text='Круто, хочу попробовать!', callback_data='menu')
    )

    return keyboard


async def get_keyboard_for_menu(activated):
    keyboard = InlineKeyboardMarkup(row_width=2)
    if activated:
        way_to_subscribe = 'Продлить подписку'
    else:
        way_to_subscribe = 'Купить'

    keyboard.row(

        InlineKeyboardButton(text=way_to_subscribe, callback_data="plans_menu"),
        InlineKeyboardButton(text="Информация", callback_data="info"),
    )

    # Create the second row of buttons on the keyboard
    keyboard.row(
        InlineKeyboardButton(text="Аккаунт", callback_data="subscription_manage"),
        InlineKeyboardButton(text="Поддержка", callback_data="support")
    )

    return keyboard


async def get_info_keyboard():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text=MESSAGES.benefits_header, callback_data="benefits"))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.FAQ, callback_data="FAQ"))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.install_button, callback_data="install"))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.plans_button, callback_data="plans_info"))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button, callback_data="menu"))
    return keyboard


async def get_support_button():
    keyboard = InlineKeyboardMarkup()
    # keyboard.add(InlineKeyboardButton(text=MESSAGES.go_to_support_button, url='https://t.me/crossbordervpn'))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button, callback_data='menu'))
    return keyboard


async def get_subscription_manage_keyboard():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text=MESSAGES.add_client_button,
                                      callback_data="how_many_client"))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.renew_subscription_button,
                                      callback_data="renew_subscription"))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.time_subscription_button,
                                      callback_data="time_subscription"))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button,
                                      callback_data="menu"))
    return keyboard


async def get_benefits():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button, callback_data="info"))
    return keyboard


async def FAQ():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button, callback_data="info"))
    return keyboard


async def get_install():
    keyboard = InlineKeyboardMarkup().add(InlineKeyboardButton(text=MESSAGES.install_phone,
                                                               callback_data="install_phone"))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.install_pc, callback_data="install_pc"))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button, callback_data="info"))
    return keyboard


async def get_menu():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text=MESSAGES.go_to_menu, callback_data="menu"))
    return keyboard


async def account_denied():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text=MESSAGES.buy_button, callback_data='plans_account'))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button, callback_data='menu'))
    return keyboard


async def subscriptions(location_of_request_to_buy):
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text=MESSAGES.one_month, callback_data='subscription_for_one_month'))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.six_months, callback_data='subscription_for_six_months'))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.twelve_months, callback_data='subscription_for_twelve_months'))

    if location_of_request_to_buy == 'menu':
        keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button, callback_data='menu'))
    elif location_of_request_to_buy == 'info':
        keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button, callback_data='info'))
    elif location_of_request_to_buy == 'account':
        keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button, callback_data='subscription_manage'))
    elif location_of_request_to_buy == 'renew_subs':
        keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button, callback_data='subscription_manage'))
    elif location_of_request_to_buy == 'subs_info':
        keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button, callback_data='subscription_info'))
    return keyboard


async def subscriptions_with_promocode(location_of_request_to_buy):
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text=MESSAGES.one_month, callback_data='subscription_for_one_month'))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.six_months, callback_data='subscription_for_six_months'))
    keyboard.add(InlineKeyboardButton(text=MESSAGES.twelve_months, callback_data='subscription_for_twelve_months'))
    keyboard.add(InlineKeyboardButton(text='У меня есть промокод', callback_data='user_has_promo'))

    if location_of_request_to_buy == 'menu':
        keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button, callback_data='menu'))
    elif location_of_request_to_buy == 'info':
        keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button, callback_data='info'))
    elif location_of_request_to_buy == 'account':
        keyboard.add(InlineKeyboardButton(text=MESSAGES.back_button, callback_data='subscription_manage'))

    return keyboard


async def choose_device_forfreeserver():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text='Телефон/Планшет', callback_data='phone_tablet_free'))
    keyboard.add(InlineKeyboardButton(text='Компьютер/Ноутбук', callback_data='desktop_laptop_free'))

    return keyboard


async def qrcode_freeserver():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text='Инструкция по установке', callback_data='instruction'))
    keyboard.add(InlineKeyboardButton(text='В главное меню', callback_data='menu'))

    return keyboard


async def recc_subs():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text='Да', callback_data='recc_subs_yes'))
    keyboard.add(InlineKeyboardButton(text='Нет', callback_data='recc_subs_not'))

    return keyboard


# <-------------- заглушка, потом убрать -------------------->
async def succesfull_buy():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text='Да', callback_data='succesfull'))

    return keyboard
# <---------------------------------------------------------->


async def choose_device():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text='Телефон/Планшет', callback_data='phone_tablet'))
    keyboard.add(InlineKeyboardButton(text='Компьютер/Ноутбук', callback_data='desktop_laptop'))
    return keyboard


async def subs_menu():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text='Управление устройствами', callback_data='device_management'))
    keyboard.add(InlineKeyboardButton(text='Инструкция по установке', callback_data='instruction'))
    keyboard.add(InlineKeyboardButton(text='В главное меню', callback_data='menu'))
    return keyboard


async def instruction_button():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text='Инструкция для пк', callback_data='instruction_pc'))
    keyboard.add(InlineKeyboardButton(text='Инструкция для смартфона', callback_data='instruction_phone'))
    keyboard.add(InlineKeyboardButton(text='Назад', callback_data='subs_menu'))

    return keyboard


async def devices(counter: int):
    keyboard = InlineKeyboardMarkup()
    for i in range(counter):
        keyboard.add(InlineKeyboardButton(text=f'Устройство {i+1}', callback_data=f'check_{i+1}_device'))
    if counter < 5:
        keyboard.add(InlineKeyboardButton(text='Подключить новое устройство', callback_data='add_new_device'))
    keyboard.add(InlineKeyboardButton(text='Назад', callback_data='subscription_info'))

    return keyboard


async def change_name(new_name: str):
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text='Поменять название', callback_data='change_name'))
    keyboard.add(InlineKeyboardButton(text='Назад', callback_data='device_management'))

    return keyboard

async def subs_ended():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text='Продлить подписку', callback_data='renew_subs'))
    keyboard.add(InlineKeyboardButton(text='В главное меню', callback_data='menu'))
    return keyboard


async def subs_info():
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text='Управление устройствами', callback_data='device_management'))
    keyboard.add(InlineKeyboardButton(text='Информация о подписке', callback_data='info_about_subscription'))
    keyboard.add(InlineKeyboardButton(text='Продлить подписку', callback_data='renew_subs'))
    keyboard.add(InlineKeyboardButton(text='Назад', callback_data='menu'))
    return keyboard

async def get_back(place: str):
    keyboard = InlineKeyboardMarkup()
    keyboard.add(InlineKeyboardButton(text='Назад', callback_data=place))
    return keyboard